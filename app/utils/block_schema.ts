export interface IBlock {
  hash: string;
  height: number;
  body: string;
  time: string;
  previousBlockHash: string;
}
